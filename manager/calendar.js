const gcal = require('google-calendar');
const Promise = require('bluebird');

const DAY = 1000 * 60 * 60 * 24;
const WEEK = DAY * 7;
const MONTH = DAY * 30;

class CalendarManager {

  static getEvents(accessToken) {
    return new Promise((resolve, reject) => {
      if (!accessToken) {
        throw "No access token available";
      }
      const google_calendar = new gcal.GoogleCalendar(accessToken);
      let userEvents = [];
      let promises = [];

      google_calendar.calendarList.list((err, calendarList) => {
        if (err) {
          console.log(err);
          throw err;
        }
        calendarList.items.map((item) => {
          const newPromise = new Promise((resolve, reject) => {
            const calendarColor = item.backgroundColor;
            google_calendar.events.list(item.id, {
              singleEvents: true,
              timeMin: new Date().toISOString(),
              timeMax: new Date(Date.now() + DAY).toISOString()
            }, (err, eventList) => {
              const calendarName = eventList.summary;
              if (eventList.items) {
                eventList.items.map((event) => {
                  const eventTime = new Date(event.start.dateTime).getTime();
                  const id = event.id;
                  const eventName = event.summary;
                  const eventDate = event.start.dateTime;
                  const color = calendarColor;
                  userEvents.push({
                    id,
                    calendarName,
                    eventName,
                    eventDate,
                    color
                  });
                });
              }
              resolve();
            });
          });
          promises.push(newPromise);
        });
        Promise.all(promises).then(() => {
          resolve(userEvents);
        });
      });
    });
  }
}

module.exports = CalendarManager;
