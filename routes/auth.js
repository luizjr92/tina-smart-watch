'use strict';

let express = require('express');
let router = express.Router();
let passport = require('../auth/auth');

router.get('/google', passport.authenticate('google', {
  scope: ['openid', 'email', 'https://www.googleapis.com/auth/calendar.readonly']
}));

router.get('/callback', passport.authenticate('google', {failureRedirect: '/login'}), (req, res) => {
  res.redirect('/');
});

router.get('/logout', (req, res) => {
  if (!req.user) {
    return res.redirect('/');
  }
  req.session.destroy();
  req.logout();
  res.redirect('/');
});

module.exports = router;
