const express = require('express');
const router = express.Router();

const CalendarManager = require('../manager/calendar');

router.get('/events', (req, res) => {
  if (!req.user) {
    return res.status(401).send("Please authenticate first");
  }
  const events = CalendarManager.getEvents(req.user.accessToken);
  events.then((list) => {
    res.status(200).json(list);
  }).catch(err => {
    res.status(500).send(err);
  });
});

module.exports = router;
