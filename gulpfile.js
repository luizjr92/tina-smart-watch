var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var sass = require('gulp-ruby-sass');
var autoprefixer = require('gulp-autoprefixer');
var jshint = require('gulp-jshint');
var livereload = require('gulp-livereload');
var webpack = require('gulp-webpack');
var clean = require('gulp-clean');

gulp.task('styles', function() {
  return sass('public/css/scss/*.scss', {style: 'expanded'}).pipe(autoprefixer('last 2 version')).pipe(gulp.dest('public/css')).pipe(livereload());
});

gulp.task('scripts', function() {
  return gulp.src('public/js/script.js').pipe(webpack({
    output: {
      filename: 'bundle.js'
    }
  })).pipe(gulp.dest('public/js')).pipe(livereload());
});

gulp.task('ejs', function() {
  return gulp.src('views/**/*.ejs').pipe(livereload());
});

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('public/css/**/*.scss', ['styles']);
  gulp.watch('public/js/script.js', ['scripts']);
  gulp.watch('views/**/*.ejs', ['ejs']);
});

gulp.task('server', function() {
  nodemon({'script': './bin/www', 'ignore': 'public/js/*.js'});
});

gulp.task('serve', ['server', 'watch']);
