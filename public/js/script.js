const $ = jQuery = require('jquery');

const FPS = 24; // frames per second

const SECOND = 1000; // 1s in ms
const MINUTE = SECOND * 60; // 1m in ms
const HOUR = MINUTE * 60; // 1h in ms
const DAY = HOUR * 24; // 1d in ms
const TIMEZONE = new Date().getTimezoneOffset();

let calendarList = [];
let viewingCalendars = [];
let eventsList = [];

const refreshCalendar = () => {
  fetchEvents();
};

const refreshDisplayedEvents = () => {
  jQuery('.event').hide();

  for (const cal of viewingCalendars) {
    jQuery(`.event[data-calendar="${cal}"]`).show();
  }
};

const registerEvents = () => {
  $('.cal-switch').change(function() {
    const elem = $(this);

    if (this.checked) {
      if (!viewingCalendars.includes(elem.data().name)) {
        viewingCalendars.push(elem.data().name);
      }
    } else {
      if (viewingCalendars.includes(elem.data().name)) {
        viewingCalendars.splice(viewingCalendars.indexOf(elem.data().name), 1);
      }
    }

    refreshDisplayedEvents();
  });
};

const generateCalendarList = () => {
  let html = "";

  for (const calendar of calendarList) {
    if (jQuery(`.cal-switch[data-name="${calendar}"]`).length == 0) {
      html += `
      <li>
        <input class="cal-switch" data-name="${calendar}" type="checkbox" checked/>
        ${calendar}
      </li>
      `;
    }
  }

  $('.calendarList').append(html);

  registerEvents();
};

const locateEvents = () => {
  jQuery('.event').each((index, element) => {
    const elem = $(element);
    const info = elem.data();
    let rotation = (info.hour / 24 * 360) + 90;
    rotation += (info.minute / 60 * (360 / 24));
    elem.css('transform', `rotate(${rotation}deg)`);
  });

  jQuery('.event').each((i, e) => {
    const elem = $(e);
    const info = elem.data();
    const hours = info.hour;
    const minutes = info.minute;
    $(`.event[data-hour="${hours}"][data-minute="${minutes}"]`).each((index, element) => {
      $(element).css('width', 45 - 2 * index + '%');
    });
  });

  setTimeout(() => {
    $('.event').each((i, e) => {
      $(e).css('top', '48.5%');
    });
  }, 300);

  generateCalendarList();
  refreshDisplayedEvents();
};

const removeMissingEvents = (data) => {
  let toRemove = eventsList.filter(item => {
    let remove = true;
    for (const event of data) {
      if (event.id == item.id && event.eventDate == item.eventDate) {
        remove = false;
      }
    }
    return remove;
  });
  toRemove.map(item => {
    jQuery(`.event[data-id="${item.id}"]`).remove();
    eventsList.splice(eventsList.indexOf(item), 1);
  });
};

const setUpEvents = (data) => {
  removeMissingEvents(data);
  for (const event of data) {
    const calName = event.calendarName;
    const name = event.eventName;
    const date = event.eventDate;
    const color = event.color;

    //Skips events without time
    if (!date) {
      continue;
    }

    //Skip rendered events
    let skip = false;
    for (const renderedEvent of eventsList) {
      if (renderedEvent.id == event.id) {
        skip = true;
        continue;
      }
    }
    if (skip) {
      continue;
    }
    eventsList.push(event);

    //Generate list with all fetched calendars
    if (!calendarList.includes(calName)) {
      calendarList.push(calName);
      viewingCalendars.push(calName);
    }

    const parsedDate = new Date(date);
    const html = `
      <div class="event" data-id=${event.id} data-calendar="${calName}" data-hour="${parsedDate.getHours()}" data-minute="${parsedDate.getMinutes()}">
        <div class="circle" title="${name} @ ${parsedDate.toLocaleTimeString()}" style="background-color: ${color}"></div>
      </div>
    `;
    $('.watch').append(html);
  }
  locateEvents();
};

const fetchEvents = () => {
  $.ajax({
    url: "/api/events"
  }).done(data => {
    setUpEvents(data);
  });
};

$(window).on('load', () => {
  setTimeout(() => {
    $('.watch').removeClass('faded');
  }, 300);
  setInterval(() => {
    let now = Date.now();
    let seconds = now % MINUTE;
    let minutes = now % HOUR;
    let hours = (now % DAY) - TIMEZONE * MINUTE;

    let percentage = seconds / MINUTE;
    let rotation = percentage * 360 - 90;
    $('.pointer.seconds').css('transform', `rotate(${rotation}deg)`);

    percentage = minutes / HOUR;
    rotation = percentage * 360 - 90;
    $('.pointer.minutes').css('transform', `rotate(${rotation}deg)`);

    percentage = hours / DAY;
    rotation = percentage * 360 + 90;
    $('.pointer.hours').css('transform', `rotate(${rotation}deg)`);

  }, (SECOND / FPS));

  fetchEvents();

  //Refreshes Events every 30 minutes
  setInterval(refreshCalendar, 30 * MINUTE);
});
